from django.conf.urls import url
from . import views

urlpatterns = [
    url(
        r'^api/icd/$',
        views.get_post_icd,
        name='get_post_icd'
    )
]