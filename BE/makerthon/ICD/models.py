from django.db import models


# Create your models here.
class ICD(models.Model):
	baso = models.CharField(max_length=255)
	eos = models.CharField(max_length=255)
	mono = models.CharField(max_length=255)
	neu = models.CharField(max_length=255)
	lym = models.CharField(max_length=255)
	wbc = models.CharField(max_length=255)
	hct = models.CharField(max_length=255)
	hgb = models.CharField(max_length=255)
	rbc = models.CharField(max_length=255)
	mch = models.CharField(max_length=255)
	mchc = models.CharField(max_length=255)
	mcv = models.CharField(max_length=255)
	mpv = models.CharField(max_length=255)
	rdw = models.CharField(max_length=255)
	pdw = models.CharField(max_length=255)
	plt = models.CharField(max_length=255)
	tpttbm = models.CharField(max_length=255)
	pct = models.CharField(max_length=255)
