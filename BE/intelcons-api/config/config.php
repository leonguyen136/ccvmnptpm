<?php

// Cau hinh Database
define("DB_HOST", "localhost"); 
define("DB_USER", "root");
define("DB_PASSWORD", "");
define("DB_DATABASE", "intelcons");

// Cau hinh Header
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: content-type");
header("Access-Control-Allow-Methods: POST, PUT, GET, DELETE");
header('Content-Type: application/json');

// Cau hinh CORS
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    var_dump(1);
    die();
}
