<?php

require_once __DIR__ . "/../../config/config.php";
require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$user_id = parsePostData($data['user_id']);
$oldPassword = parsePostData($data['oldPassword']);
$newPassword = parsePostData($data['newPassword']);


// get old pass word in DB
$sqlPassword = "
        SELECT 
            password
        FROM 
            users
        WHERE 
            id = '$user_id'
    ";
$resultPassword = fetchArray($sqlPassword);
$oldPasswordHash = $resultPassword[0]["password"];

if (password_verify($oldPassword, $oldPasswordHash)) {
    // hash new password
    $newPasswordHash = password_hash($newPassword, PASSWORD_BCRYPT);
    $sql = "
        UPDATE 
            users 
        SET 
            password = '$newPasswordHash'
        WHERE
            id = '$user_id'
    ";

    $result = commit($sql);
    apiResponse('Thành công', 200, $result);
} else {
    apiResponse('Mật khẩu cũ không chính xác', 203, []);
}
