<?php

require_once __DIR__ . "/../../config/config.php";
require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$fullname = parsePostData($data['fullname']);
$username = parsePostData($data['username']);
$password = parsePostData($data['password']);
$passwordHash = password_hash($password, PASSWORD_BCRYPT);
$phone = parsePostData($data['phone']);
$address = parsePostData($data['address']);
$gender = parsePostData($data['gender']);
$identify = parsePostData($data['identify']);
$age = parsePostData($data['age']);
$gaurdian = parsePostData($data['gaurdian']);
$gaurdian_phone = parsePostData($data['gaurdian_phone']);


// check username
$sqlUsername = "
    SELECT id
    FROM users
    WHERE username = '$username'
";
$resultUsername = fetchArray($sqlUsername);

if($resultUsername) {
    apiResponse('Tên tài khoản đã tồn tại. Vui lòng nhập tên tài khoản khác !', 203, []);
} else {
    $sql = "
    INSERT INTO users ( fullname, username, password, phone, address, gender, identify, age, gaurdian, gaurdian_phone )
    VALUES
        ('$fullname', '$username', '$passwordHash', '$phone', '$address', '$gender', '$identify', '$age', '$gaurdian', '$gaurdian_phone')
    ";

    $result = commit($sql);

    if($result) {
        $sqlGetNewUser = "
            SELECT
                id
            FROM
                users 
            ORDER BY
                id DESC 
            LIMIT 1
        ";

        $resultGetNewUser = fetchArray($sqlGetNewUser);
        
        if($resultGetNewUser) {
            apiResponse('Thành công', 200, $resultGetNewUser);
        } else {
            apiResponse('Thất bại', 203, []);
        }
    }
}
