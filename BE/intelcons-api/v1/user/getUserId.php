<?php

require_once __DIR__ . "/../../config/config.php";
require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$user_id = parsePostData($data['user_id']);

$sql = "
    SELECT
        id,
        fullname,
        phone, 
        address, 
        gender,
        identify,
        age,
        gaurdian,
        gaurdian_phone,
        height, 
        weight,
        blood_type
    FROM
        users
    WHERE
        id = '$user_id'
";

$result = fetchArray($sql);

if($result) {
    apiResponse('Thành công', 200, $result);
} else {
    apiResponse('Thất bại', 203, []);
}