<?php

require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$user_id = parsePostData($data['user_id']);
$fullname = parsePostData($data['fullname']);
$phone = parsePostData($data['phone']);
$address = parsePostData($data['address']);
$gender = parsePostData($data['gender']);
$age = parsePostData($data['age']);
$gaurdian = parsePostData($data['gaurdian']);
$gaurdian_phone = parsePostData($data['gaurdian_phone']);
$height = parsePostData($data['height']);
$weight = parsePostData($data['weight']);
$blood_type = parsePostData($data['blood_type']);

$sql = "
    UPDATE 
        users
    SET 
        fullname = '$fullname',
        phone = '$phone',
        address = '$address',
        gender = '$gender',
        age = '$age',
        gaurdian = '$gaurdian',
        gaurdian_phone = '$gaurdian_phone',
        height = '$height',
        weight = '$weight',
        blood_type = '$blood_type'
    WHERE
        id = '$user_id'
";

$result = commit($sql);

apiResponse('Thành công', 200, $result);