<?php

require_once __DIR__ . "/../../config/config.php";
require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$username = parsePostData($data['username']);
$password = parsePostData($data['password']);

$sqlUsername = "
    SELECT 
        id,
        fullname,
        phone, 
        address, 
        gender,
        identify,
        age,
        gaurdian,
        gaurdian_phone
    FROM users
    WHERE username = '$username'
";
$resultUsername = fetchArray($sqlUsername);

if($resultUsername) {
    $sqlPassword = "
        SELECT password
        FROM users
        WHERE username = '$username'
    ";
    $resultPassword = fetchArray($sqlPassword);
    $passwordHash = $resultPassword[0]["password"];

    if (password_verify($password, $passwordHash)) {
        apiResponse('Đăng nhập thành công', 200, $resultUsername);
    } else {
        apiResponse('Đăng nhập thất bại', 203, []);
    }
} else {
    apiResponse('Đăng nhập thất bại', 203, []);
}
