<?php

require_once __DIR__ . "/../../config/config.php";
require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$predict_record_id = parsePostData($data['predict_record_id']);

$sql = "
    SELECT
        d.id,
        d.fullname,
        d.phone,
        d.address,
        d.image,
        d.location_id,
        m.name AS department,
        h.name AS hospital,
        d.longitude,
        d.latitude 
    FROM
        predict_detail_doctors p,
        doctors d,
        departments m,
        hospitals h 
    WHERE
        predict_id = '$predict_record_id'
        AND p.doctor_id = d.id
        AND d.department_id = m.id 
        AND d.hospital_id = h.id
    ";

$result = fetchArray($sql);

if($result) {
    apiResponse('Thành công', 200, $result);
} else {
    apiResponse('Thất bại', 203, []);
}