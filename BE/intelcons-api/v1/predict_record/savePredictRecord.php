<?php

require_once __DIR__ . "/../../config/config.php";
require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$baso = parsePostData($data['baso']);
$eos = parsePostData($data['eos']);
$mono = parsePostData($data['mono']);
$neu = parsePostData($data['neu']);
$lym = parsePostData($data['lym']);
$wbc = parsePostData($data['wbc']);
$hct = parsePostData($data['hct']);
$hgb = parsePostData($data['hgb']);
$rbc = parsePostData($data['rbc']);
$mch = parsePostData($data['mch']);
$mchc = parsePostData($data['mchc']);
$mcv = parsePostData($data['mcv']);
$mpv = parsePostData($data['mpv']);
$rdw = parsePostData($data['rdw']);
$pdw = parsePostData($data['pdw']);
$plt = parsePostData($data['plt']);
$tpttbm = parsePostData($data['tpttbm']);
$pct = parsePostData($data['pct']);
$predict = parsePostData($data['predict']);
$timestamp = parsePostData($data['timestamp']);
$user_id = parsePostData($data['user_id']);

$sql = "
    INSERT INTO predict_records ( baso, eos, mono, neu, lym, wbc, hct, hgb, rbc, mch, mchc, mcv, mpv, rdw, pdw, plt, tpttbm, pct, predict, timestamp, user_id)
    VALUES
        ('$baso', '$eos', '$mono', '$neu', '$lym', '$wbc', '$hct', '$hgb', '$rbc', '$mch', '$mchc', '$mcv', '$mpv', '$rdw', '$pdw', '$plt', '$tpttbm', '$pct', '$predict', '$timestamp', '$user_id')
    ";

$result = commit($sql);

if($result) {
    $sqlNewRecord = "
        SELECT
            id
        FROM
            predict_records 
        ORDER BY
            id DESC 
        LIMIT 1
    ";

    $resultGetNewRecord = fetchArray($sqlNewRecord);
    
    if($resultGetNewRecord) {
        apiResponse('Thành công', 200, $resultGetNewRecord);
    } else {
        apiResponse('Thất bại', 203, []);
    }
}