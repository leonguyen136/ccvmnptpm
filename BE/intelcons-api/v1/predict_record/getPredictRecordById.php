<?php

require_once __DIR__ . "/../../config/config.php";
require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$user_id = parsePostData($data['user_id']);

$sql = "
    SELECT
        *
    FROM
        predict_records
    WHERE
        user_id = '$user_id'
";

$result = fetchArray($sql);

if($result) {
    apiResponse('Thành công', 200, $result);
} else {
    apiResponse('Thất bại', 203, []);
}