<?php

require_once __DIR__ . "/../../config/config.php";
require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$predict_record_id = parsePostData($data['predict_record_id']);

$sql = "
    SELECT
        h.id as hospital_id,
        h.name,
        h.address,
        h.hotline,
        h.image,
        h.location_id,
        h.longitude,
        h.latitude,
        h.ranking
    FROM
        predict_detail_hospitals p, hospitals h
    WHERE
        p.predict_id = '$predict_record_id'
        AND p.hospital_id = h.id
";

$result = fetchArray($sql);

if($result) {
    apiResponse('Thành công', 200, $result);
} else {
    apiResponse('Thất bại', 203, []);
}