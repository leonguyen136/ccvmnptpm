<?php

require_once __DIR__ . "/../../config/config.php";
require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$predict_record_id = parsePostData($data['predict_record_id']);
$doctor_id = parsePostData($data['doctor_id']);

$sql = "
    INSERT INTO predict_detail_doctors (predict_id, doctor_id)
    VALUES
        ('$predict_record_id', '$doctor_id')
    ";

$result = commit($sql);

apiResponse('Thành công', 200, $result);