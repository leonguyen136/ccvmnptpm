<?php

require_once __DIR__ . "/../../config/config.php";
require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$sql = "
    SELECT
        *
    FROM
        locations
";

$result = fetchArray($sql);

if($result) {
    apiResponse('Thành công', 200, $result);
} else {
    apiResponse('Thất bại', 203, []);
}