<?php

require_once __DIR__ . "/../../config/config.php";
require_once __DIR__ . "/../../src/database.php";
require_once __DIR__ . "/../../src/core.php";

$data = json_decode(file_get_contents('php://input'), true);

$sql = "
    SELECT
        d.id,
        d.fullname,
        d.phone,
        d.address,
        d.image,
        d.location_id,
        m.name as department,
        h.name as hospital,
        d.longitude,
        d.latitude
    FROM
        doctors d, departments m, hospitals h
    WHERE
        d.department_id = m.id AND d.hospital_id = h.id
";
 
$result = fetchArray($sql);

if($result) {
    apiResponse('Thành công', 200, $result);
} else {
    apiResponse('Thất bại', 203, []);
}