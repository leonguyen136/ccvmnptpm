<?php

function apiResponse($message, $status, $data) {
    http_response_code($status);
    $response = [
        'message' => $message,
        'status' => $status,
        'data' => $data
    ];

    echo json_encode($response);
}

function parsePostData($data) {
    return isset($data) ? $data : NULL;
}