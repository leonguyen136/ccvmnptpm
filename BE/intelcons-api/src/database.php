<?php

/**
 * Ket noi database
 *
 * @copyright 2020 by IntelCons
 * @author Nguyễn Thanh Hào <thanhhao130699@gmail.com>
 */

require_once __DIR__ . "./../config/config.php";

/**
 * Ket noi database
 *
 * @param null
 * @return mysqli
 */

/**
 *  Ham ket noi database
 */
function connect()
{
    $link = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE);

    if (!$link) {
        var_dump("Có lỗi: " . mysqli_connect_errno() . PHP_EOL);
        var_dump("Có lỗi: " . mysqli_connect_error() . PHP_EOL);
        var_dump("Lỗi kết nối");
        die();
    }

    mysqli_set_charset($link, "utf8");
    return $link;
}

/**
 *  Ham ngat ket noi database
 */
function disconnect($link)
{
    mysqli_close($link);
}

/**
 *  Ham tra ve du lieu
 */
function fetchArray($sql, $type = MYSQLI_ASSOC)
{
    $data = array();
    $link = connect();

    mysqli_query($link, "SET sql_mode = ''");
    $result = mysqli_query($link, $sql);

    if (!$result) {
        var_dump("Lỗi truy vấn: " . mysqli_error($link));
        var_dump("\n\nCâu lệnh truy vấn: " . $sql);
        die();
    }

    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_array($result, $type)) {
            $data[] = $row;
        }
    }

    mysqli_free_result($result);
    disconnect($link);
    return $data;
}

/**
 *  Ham tra khong tra ve du lieu
 */
function commit($sql)
{
    $link = connect();

    $result = mysqli_query($link, $sql);

    if (!$result) {
        var_dump("Lỗi truy vấn: " . mysqli_error($link));
        var_dump("\n\nCâu lệnh truy vấn: " . $sql);
        die();
    }

    disconnect($link);
    return $result;
}
